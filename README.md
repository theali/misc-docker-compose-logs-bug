## Bug

When dependent services are started in background, and logs are displayed only from main service, the background service
may hit some limit of output log size and hung on writting to stdout.

## Steps to reproduce

1. Build and start `service_b`:

```
docker-compose up --build service_b
```

2. Tail logs of `service_a` in separate console:

```
docker-compose logs -f service_a
```

The logs from `service_a` should stop flowing before reaching the final line.
