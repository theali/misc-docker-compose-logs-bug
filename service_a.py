import time
import datetime

print('Service A: waiting 5s...')
time.sleep(5)

for i in range(100000):
	print(f'Service A, line {i}, date: {datetime.datetime.now().isoformat()}')

print('done')
time.sleep(5)
